import random

def generar_contrasena():
    mayusculas = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
    minusculas = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    simbolos = ['!', '$', '&', '*', '#']
    numeros = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    
    lista_insumo = mayusculas + minusculas + simbolos + numeros
    contrasena = []

    for i in range(15):
        caracter_random = random.choice(lista_insumo)
        contrasena.append(caracter_random)
    
    # Me ayuda a transformar una lista en texto, string
    contrasena = ''.join(contrasena)
    return contrasena


def run():
    contrasena = generar_contrasena()
    print(f'Tu nueva contraseña es: {contrasena}')


if __name__ == '__main__':
    run()
