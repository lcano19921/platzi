#---------------- Ejercicio completo --------------
menu = '''
Bienvenido al conversor de monedas 💲

1 - Pesos Colombianos
2 - Pesos Argentinos
3 - Pesos Mexicanos

Elige una opcion
'''

option = int(input(menu))

if option == 1:
    valor_dolar = 3875
    moneda = 'Pesos Colombianos'
elif option == 2:
    valor_dolar = 65
    moneda = 'Pesos Argentinos'
elif option == 3:
    valor_dolar = 24
    moneda = 'Pesos Mexicanos'
else:
    print('Elige una opcion valida')

pesos = float(input(f'Porfavor ingrese en {moneda} la cantidad que desea convertir: '))
pesos = round(pesos)
dolares = round((pesos/valor_dolar),2)
print(f'Los {pesos} {moneda}, equivalen a {dolares} dolares')


#--------------------------- Ejercicio Basico ---------------------------
# pesos = input('Porfavor ingrese en pesos colombianos la cantidad que desea convertir: ')
# pesos = float(pesos)
# valor_dolar = 3875
# dolares = round((pesos / valor_dolar),2)
# print(f'Los {pesos} pesos, equivalen a {dolares} dolares')