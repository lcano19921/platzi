def run():
    mi_diccionario = {
        'llave1': 1,
        'llave2': 2,
        'llave3': 3
    }
    # print(mi_diccionario['llave1'])
    # print(mi_diccionario['llave2'])
    # print(mi_diccionario['llave3'])


    poblacion_paises = {
        'Argentina': 44_345_345,
        'Colombia': 50_456_765,
        'Brasil': 45_456_987
    }

    # '''Usar el metodos keys, muestra la lista de las llaves'''
    # for pais in poblacion_paises.keys():
    #     print(pais)

    # '''Usar el metodos values, muestra la lista de los valores'''
    # for pais in poblacion_paises.values():
    #     print(pais)

    # '''Usar el metodos items, muestra la lista de las llaves y los valores'''
    # for pais, poblacion in poblacion_paises.items():
    #     print(f'{pais} tiene {poblacion} habitantes')

    for pais in poblacion_paises:
        print(pais)

    # print(poblacion_paises['Bolivia'])



if __name__ == '__main__':
    run()