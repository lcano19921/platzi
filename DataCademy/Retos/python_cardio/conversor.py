'''Conversor de millas'''
#Imagina que quieres calcular los kilómetros que son cierta cantidad de millas. Para no estar repitiendo este cálculo escribe un 
#programa en que el usuario indique una cantidad de millas y en pantalla se muestre el resultado convertido a kilómetros.
#Toma en cuenta que en cada milla hay 1.609344 Km
#Bonus: haz que el usuario pueda escoger entre convertir millas a kilómetros o kilómetros a millas.

def convertir_millas(millas):
    '''Funcion que vonvierte millas a kilometros '''
    result = millas * 1.609344

    return result


def convertir_kilometros(kilometros):
    '''Funcion que vonvierte kilometros a millas '''
    result = kilometros * 0.621371

    return result


def conversor(opcion):
    '''Funcion que maneja las opciones de usuario para convertir medidas'''
    if opcion == '1':
        unidad = float(input('Porfavor ingresa las millas que deseas convertir: '))
        if unidad <= 0:
            #Se valida que sigite una opcion adecuada
            unidad = float(input('Porfavor ingrese una cantidad valida de millas: '))
            # Se llama la funcion encargada de la conversion en el caso de las millas
            dato = convertir_millas(unidad)
        else:
             # Se llama la funcion encargada de la conversion en el caso de las millas
            dato = convertir_millas(unidad)
    else:
        unidad = float(input('Porfavor ingresa los kilometros que deseas convertir: '))
        if unidad <= 0:
            #Se valida que sigite una opcion adecuada
            unidad = float(input('Porfavor ingrese una cantidad valida de kilometros: '))
            # Se llama la funcion encargada de la conversion en el caso de los kilometros
            dato = convertir_millas(unidad)
        else:
            # Se llama la funcion encargada de la conversion en el caso de los kilometros
            dato = convertir_kilometros(unidad)
        
    return unidad, dato



def run():
    menu = '''Bienvenido a el conversor de medidas, elige lo que deseas:
    1. Millas a Kilometros
    2. Kilometros a Millas 
    ->'''
    opcion = input(menu)

    sw = 0
    while sw <= 1:
        '''While que asegura digiten una opcion valida'''
        if opcion != '1' and opcion != '2':
            opcion = input('opcion invalida, recuerda que debes digitar 1 o 2: ')
            print(sw)
            sw +=1
        else:
            break
    
    if sw == 2:
        print('Lo sentimos, no digitaste una opcion valida')
    else:
        transformacion = conversor(opcion)
        if opcion == '1':
            print(f'''-------------------
            Conversion solicitada: Millas a Kilometros
            Millas ingresadas: {transformacion[0]}
            Las millas ingresadas corresponden a {transformacion[1]} kilometros''')
        else:
            print(f'''-------------------
            Conversion solicitada: Kilometros a MIllas
            Kilometros ingresados: {transformacion[0]}
            Los Kilometros ingresados corresponden a {transformacion[1]} Millas''')
        


if __name__ == '__main__':
    run()