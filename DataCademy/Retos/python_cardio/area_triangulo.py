'''Calcular area de un triangulo'''
# Es momento de poner ese conocimiento a la práctica. El área de un triángulo se describe de la siguiente manera: A = (b * h) / 2 .
# Escribe un programa que tome la base y la altura como parámetros y calcule el área del triángulo.
# Bonus: el programa debe determinar si el triángulo es isósceles, equilátero o escaleno.

def area_triangulo(base,altura):
    '''Funcion que calcula el area del triangulo'''
    area = (base*altura)/2
    return area


def tipo_triangulo(base,altura,hipotenusa):
    '''Funcion para evaluar si un triangulo es isósceles, equilátero o escaleno'''
    #Evalua los lados segun la regla:    
    # Equilátero Los tres lados son de igual tamaño.    
    if base == altura and altura == hipotenusa:
        tipo = 'Equilátero'
    # Escaleno	Los tres lados tienen medidas diferentes.
    elif base != altura and altura != hipotenusa and base != hipotenusa:
        tipo = 'Escaleno'
    # Isósceles	Dos lados de igual tamaño.
    else:
        tipo = 'Isósceles'        
    
    return tipo
    

def run():
    base = float(input('Porfavor ingrese la base del triangulo: '))
    altura = float(input('Porfavor ingrese la altura del triangulo: '))
    hipotenusa = input('Si desea saber que tipo de triangulo es, ingrese la hipotenusa, de lo contrario presione enter: ')
    if hipotenusa != '':
        hipotenusa = float(hipotenusa)
        mensaje_triangulo = f' y teniendo en cuenta su hipotenusa de {hipotenusa} se trata de un triangulo {tipo_triangulo(base,altura,hipotenusa)}'
    else:
        mensaje_triangulo = ''
    area = area_triangulo(base,altura)
    print(f'El area de untriangulo con base {base} y altura {altura} es de {area}{mensaje_triangulo}')


if __name__ == '__main__':
    run()