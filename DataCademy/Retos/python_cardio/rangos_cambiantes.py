'''Rangos Cambiantes'''
#Para este reto final juguemos con números. En tu programa pide al usuario ingresar 3 números: un límite inferior, 
#un límite superior y uno de comparación.
#Si tu número de comparación se encuentra en el rango de los dos límites, imprímelo en pantalla.
#En caso de estar por debajo del inferior o arriba del superior, también muéstralo en pantalla y 
#pide ingresar otro número para repetir el proceso.


def comparacion(infe,supe,numero):
    if numero >= infe and numero <= supe:
        estado = 'numero apto'
    elif numero < infe:
        estado = 'numero_debajo'
    else:
        estado = 'numero_arriba'
    
    return estado


def run():
    linferior = int(input('Porfavor ingresa el limite inferior a establecer '))
    lsperior = int(input('Porfavor ingresa el limite superior a establecer '))
    numero = int(input('Porfavor ingresa el numero a evaludar '))
    print(f'ingresaste un {comparacion(linferior,lsperior,numero)}')


if __name__ == '__main__':
    run()

