'''Juego piedra papel o tijera'''
# Este es un juego en el que nunca fui bueno, pero eso no significa que hacer un programa sea difícil. Escribe un programa que reciba como parámetro “piedra”, “papel”, o “tijera” y determine si ganó el jugador 1 o el jugador 2.
# Bonus: determina que el ganador sea el jugador que gane 2 de 3 encuentros.
# Ejemplo:
# ppt(“piedra”, “papel”) ➞ “El ganador es el jugador 2

def comparacion(elec_jugador1, elec_jugador2):  
    '''Funcion que compara las 9 posibles combinaciones y determina 
    si hay un empate, o quien es el ganador y devuelve este resultado'''  
    
    if elec_jugador1 == 'piedra' and elec_jugador2 == 'piedra':
        ganador = 'empate'
    elif elec_jugador1 == 'piedra' and elec_jugador2 == 'papel':
        ganador = 'jugador2'
    elif elec_jugador1 == 'piedra' and elec_jugador2 == 'tijera':
        ganador = 'jugador1'
    elif elec_jugador1 == 'papel' and elec_jugador2 == 'papel':
        ganador = 'empate'
    elif elec_jugador1 == 'papel' and elec_jugador2 == 'piedra':
        ganador = 'jugador1'
    elif elec_jugador1 == 'papel' and elec_jugador2 == 'tijera':
        ganador = 'jugador2'
    elif elec_jugador1 == 'tijera' and elec_jugador2 == 'tijera':
        ganador = 'empate'
    elif elec_jugador1 == 'tijera' and elec_jugador2 == 'piedra':
        ganador = 'jugador2'
    elif elec_jugador1 == 'tijera' and elec_jugador2 == 'papel':
        ganador = 'jugador1'
        
    return ganador


def run():
    #Se inicial el juego con el mensaje de bienvenida y tomando los nombres de los jugadores
    jugador1 = input('Bienvenido al juego piedra, papel o tijera, porfavor ingresa el nombre del jugador 1: ')
    jugador2 = input('Ahora ingresa el nombre del jugador 2:  ')
    menu = '''Porfavor digita tu opcion:
    1. Piedra
    2. Papel
    3. Tijera'''

    #Se inicializan las variables necesarias para los contadores
    cont_juegos = 0
    cont_jugador1 = 0
    cont_jugador2 = 0
    cont_empate = 0
    cont_turnos = 0

    while cont_juegos < 3:
        '''Este while garantiza que el juego se repita hasta que exista un ganador con dos victorias sobre 3 juegos
        y no cuenta los empates, es decir que solo tiene en cuenta como juego, cuando uno de los dos jugadores gana'''
        sw = 0
        while sw <= 1:
            '''Validamos que ingresen una opcion adecuada (piedra, papel, o tijera)'''
            elec_jugador1 = input(f'{jugador1} {menu} ')
            #Normalizamos la informacion para tener todo en minuscula
            elec_jugador1 = elec_jugador1.lower()

            #Validamos su la opcion ingresada es correcta
            if elec_jugador1 != 'piedra' and elec_jugador1 != 'tijera' and elec_jugador1 != 'papel':
                print(f'{elec_jugador1} no es una opcion valida')
                sw +=1
            else:
                #En caso de que la opcion si sea correcta nos salimos del while
                break
        
        #Si el usuario ha ingresado 2 opciones erradas -- en el turno -- terminamos el juego
        if sw == 2:
            responsable = jugador1
            break
        
        sw2 = 0
        while sw2 <= 1:
            '''Validamos que ingresen una opcion adecuada (piedra, papel, o tijera)'''
            elec_jugador2 = input(f'{jugador2} {menu} ')
            #Normalizamos la informacion para tener todo en minuscula
            elec_jugador2 = elec_jugador2.lower()
    
            if elec_jugador2 != 'piedra' and elec_jugador2 != 'tijera' and elec_jugador2 != 'papel':
                print(f'{elec_jugador2} no es una opcion valida')
                sw2 +=1
            else:
                #En caso de que la opcion si sea correcta nos salimos del while
                break
        
        #Si el usuario ha ingresado 2 opciones erradas -- en el turno -- terminamos el juego
        if sw2 == 2:
            responsable = jugador2
            break
        
        # Luego de todas las validaciones, llamamos la funcion comparacion para saber quien ganó en este turno
        ganador = comparacion(elec_jugador1,elec_jugador2)

        # En este bloque validamos el return de comparacion, y segun ello mostramos el resultado del intento
        # y asignamos los contadores pertinentes
        if ganador == 'jugador1':
            print(f'En este turno el ganador es {jugador1}, ya que {elec_jugador1} vence a {elec_jugador2}')
            print('--------------------')
            cont_jugador1 += 1
            cont_juegos += 1
        elif ganador == 'jugador2':
            print(f'En este turno el ganador es {jugador2}, ya que {elec_jugador2} vence a {elec_jugador1}')
            print('--------------------')
            cont_jugador2 += 1
            cont_juegos += 1
        elif ganador == 'empate':
            print(f'En este turno hay un empate, ya que {elec_jugador1} = {elec_jugador2}')
            print('--------------------')
            #Aqui no se incrementa el cont_juegos dado que el empate no se cuenta como juego
            cont_empate += 1
        cont_turnos +=1
    
    if sw == 2 or sw2 == 2:
        # Validamos si no se han digita opciones erradas que terminen el juego
        print(f'El juego ha terminado dado que {responsable} ha ingresado varias opciones erradas')
    else:
        # En caso de no tener opciones invalidas se valida quien el fue el ganador final
        if cont_jugador1 > cont_jugador2:
            ganador_final = jugador1
        else:
            ganador_final = jugador2

        #Se imprime el resumen del juego
        print(f'''--------------------
        Resumen del juego
        Turnos Jugados: {cont_turnos} 
        Empates: {cont_empate}
        Victorias de {jugador1}: {cont_jugador1}
        Victorias de {jugador2}: {cont_jugador2}
        ¡¡¡ GANADOR !!! == > {ganador_final}''')
 
        
if __name__ == '__main__':
    run()