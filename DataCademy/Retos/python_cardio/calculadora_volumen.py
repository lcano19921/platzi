'''Calculadora de Volumen'''
#Sigamos con matemáticas elementales para no perder la costumbre y retar nuestras habilidades. Escribe un programa donde apliques 
#las diferentes fórmulas matemáticas para calcular el volumen de un cilindro.
#Recuerda que la base del cilindro es un círculo y necesitarás calcular su área. Aplica las fórmulas en tu programa recibiendo 
#datos como altura y radio.
#Bonus: agrega otras figuras geométricas a tu programa y que el usuario pueda escoger cuál calcular.

import math

def volumen_esfera():
    radio = float(input('ingrese el radio de la esfera '))
    volumen =(math.pi*4*(radio**2))/3

    return volumen


def volumen_cono():
    base = float(input('Ingrese la base del cono '))
    altura = float(input('Ingrese la altura del cono '))
    volumen = (base * altura)/3

    return volumen


def volumen_cilindro():
    radio = float(input('Ingrese el radio el cilindro '))
    altura = float(input('ingrese la altura del cilindro '))
    volumen =(math.pi*(radio**2)*altura)

    return volumen


def volumenes(opcion):
    if opcion == '1':
        volumen = volumen_cilindro()
    elif opcion == '2':
        volumen = volumen_cono()
    elif opcion == '3':
        volumen = volumen_esfera()
   

    return volumen



def run():
   menu = '''Porfavor indica la figura para la cual deseas obtener el volumen:
   1. Circulo
   2. Cono
   3. Esfera   
   -> '''
   opcion = input(menu)
   volumen = volumenes(opcion)

   print (f'El volumen es: {volumen}')

   



if __name__ == '__main__':
    run()