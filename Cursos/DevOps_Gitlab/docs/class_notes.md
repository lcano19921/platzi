# Notas de Clase

El DevOps es el encargado de que todo el proceso de salida a produccion del codigo esten supremamente bien.

***Pruebas Automatizadas:***
Siempre debemos tener pruebas unitarias, y utilizar *Test Driver Development*

***Continuos Integration (CI):***
Esta integracion continua nos permite enviar codigo continuamente para mejorar la calidad del software

***MOnitoreo y logueo:***
Se suguiere trabajar en microservicios para poder tener las unidades lo más pequeña posibles, pues fscilita la seguridad y reduce la probabilidad de bugs

***Ciclo de vida:***
Empaquetar se puede hacer perfectamente en docker.
Sacamos una nueva version del código y siempre se encuentra en un feature flask

***Beneficios:***
Velocidad ya que las cosas manuales, como los scripts y la infraestructura son mejor.

Rapida distribucion del código

Mayor confiabilidad, ya que continuamente probamos que el codigo tiene un performance mucho mejor

La escalabilidad que nos permite realizar es impresionante, dado que con docker y cloud run por ejemplo mejoran mucho esto.

Herramientas de colaboracion

Mejora la seguridad

## **GITLAB**

Es especializado en DevOps y tiene muchisimas funcionalidades

***Administracion:***
Permite autentificacion por varios metodos como SSH

***Planificacion:***
Nos permite hacer planificacion, es especialista en el desarrollo agil, por lo tanto crear milestone, issies y demás

***Creacion:***
Podemos crear merge request, grupos, proyectos y roles

***Verificacion:***
Podemos montar pruebas de seguridad, calidad, review...
- Gitlab CI

***Empaquetacion:***
Empaquetar es generar el codigo para que este listo, es decir, compilaciones, conversiones, etc...

***Distribucion (Release):***
Varias opciones de deploy y ambientes

***Configuracion:***
Trabaja fuertemente de la mano con kubernetes

***Monitoreo:***
Lo hace de la mano con Prometheus, Jeager y Sentry

***Seguridad:***
Ofrece mucha seguridad, y se deben tomar muchas medidas para ello, pues se pueden realizar pruebas dinamicas y estaticas del codigo, para detectar practicas inseguras y demás elementos

***Defensa:***
Firewall propios incluso

***Autenticarse:***
Nos podemos autenticar con user/name

En otro nivel esta el OTP

El mas recomendado es el SSH key, esto previene el fishing para evitar atques de replay atack

Llaves PGP para firmar todos los comits

**APPS RECOMENDADAS PARA TWO FACTOR AUTENTICATION**
-  Google autenticator
- Authy 
- Yubico

**GRUPOS**

Permiten compartir recursos entre varios miembros del equipo y poder organizar la forma en que trabajamos; permiten:

Agrupar proyectos
Otorgar permisos de acceso
Compartir recursos
Subgrupos: permiten modelar la forma en la que nuestra compañia está diseñada (2 equipos de Dev, 1 Diseñadores, 1 Directivos)

Los grupos se utilizan en GitLab a traves de “NAMESPACES”, lo que nos dará una url única para usuario, nombre de grupo y/o subgrupos

Nombres reservados para los grupos, las palabras reservadas son: https://docs.gitlab.com/ee/user/reserved_names.html

3 tipos de visibilidad:

1- Pública: cualquier usuario puede ver los contenidos de ese grupo
2- Interna: adentro de la instancia de GitLab (usuarios logueados) pueden tener acceso a ese grupo
3- Privada: Proyectos y grupos sean accesibles a traves de los permisos que se asignan en GitLab

## **Permisos de Grupo (autorización)**
- Guest . Solo Lectura.
- Reporter pueder abrir y comentar issue.
- Developer Subir código, acceso a issue, branch.
- Owner / Maintainer. Puede realizar configuraciones del proyecto + Dev. (Owner→creador del proyecto. Maintainer→no creador del proyecto)

## **Proyectos de gitlab**
- Issue tracker (desarrollo agil)
- repositorio de codigo
- Gitlab CI - Integracion continua
---

## **METODOLOGIA AGIL** Mucho mejor que wanderfall

se realizan entregas de sprints
- Sprint (partes pequeñas del proyecto)
- Pruebas
- Release
- Se repite el ciclo

## Boards
Los boards son una forma de visualizar los flujos de trabajo, de ver quién está trabajando en qué issues y son unas de las herramientas más importantes que existen dentro de Gitlab.

Se puede utilizar para Kanban o Scrum.
Une los mundos de los issue tracking y Project managment.

## Service Desk
El Service Desk es la capacidad que te da Gitlab de abrir issues a través de correo electrónico.

Permite dar soporte a través de email a tus clientes directamente desde Gitlab.
Permite que el equipo no técnico reporte bugs o abra issue sin necesidad de que tengan una cuenta de Gitlab.
Cuando se activa el servicio, se genera un email único para el proyecto.

## Quick Actions
Las Quick Actions son atajos textuales para acciones comunes en issues, epics, merge request y commits que normalmente son ejecutadas a través de la UI de Gitlab. Los comandos se pueden agregar al momento de crear un issue o un merge request o en los comentarios de los mismos. Cada comando debe incluirse en una línea separada para que se detecten y ejecuten. Una vez ejecutados, los comandos se retiran del texto y no pueden verse en el comentario o descripción del issue.

Estos son tan sólo unos ejemplos de los Quick Actions que Gitlab ofrece. Gitlab añade Quick Actions todo el tiempo, por lo que te recomiendo familiarizarte con la documentación y consultarla constantemente. Aquí la lista completa: https://docs.gitlab.com/ee/user/project/quick_actions.html

## Cherry-pick
Cherry-pick es para pasar solo commits especificos de una rama a otra, sin hacer un merge de un conjunto de commits

Por ejemplo si la rama de desarrollo tiene 3 commits nuevos y solo necesitamos pasas el commit intermedio a master, cherry-pick permite pasar solo este commit y dejar “pendiente” el resto

Seleccionando commits: Cherry-pick

Cherry-pick changes - Gitlab

## CI

El Continuous Integration es una práctica en la que los desarrolladores envían sus cambios a un repositorio central, lo cual detona builds y pruebas automatizadas.

Ayuda a encontrar bugs de manera oportuna
Aumenta la velocidad de los releases
Automatiza el pipeline que lleva código desde la computadora del desarrollador hasta el dispositivo del cliente.

Gitlab CI es el hub central de automatización de Gitlab, es el pedazo que podemos configurar libremente para generar las automatizaciones necesarias para que nuestro flujo de trabajo requiera poca o ninguna interacción humana.

Continuamente construye, prueba y despliega cambios pequeños al código.
Se configura con el archivo gitlab-ci.yml
También nos permite realizar Continuous Delivery y Continuous Deployment.

Verificar
Crear y probar automáticamente la aplicación con la integración continua.
Analizar la calidad del código con GitLab Code Quality.
Determinar el impacto en el rendimiento de los cambios de código con las Prueba de rendimiento en el navegador.
Realice una serie de pruebas, como Escaneo de contenedores, [Escaneo de dependencias](https://docs.gitlab.com/ee/user/
application_security/dependency_scanning/index.html), y JUnit tests.
Implemente sus cambios con [Revisar aplicaciones] (https://docs.gitlab.com/ee/ci/review_apps/index.html) para obtener una vista previa de los cambios de la aplicación en cada sucursal.
Empaquetar
Almacenar las imágenes de Docker con Container Registry.
Almacene paquetes NPM con NPM Registry.
Almacene los repositorios Maven con Repositorio Maven.
Almacene Conan Repositories con Conan Repository.
Lanzar
Continuous Deployment, implementando automáticamente la aplicación en producción.
Continuous Delivery, haga clic manualmente para implementar su aplicación en producción.
Implemente sitios web estáticos con GitLab Pages.
Envíe funciones a solo una parte de sus pods y permita que un porcentaje de su base de usuarios visite la función implementada temporalmente con Canary Deployments.
Implementación de funciones detrás de Indicadores de funciones.
Agregue notas de lanzamiento a cualquier etiqueta de Git con GitLab Releases.
Vista del estado actual y el estado de cada entorno de CI que se ejecuta en Kubernetes con Deploy Boards.
Implementa tu aplicación en un entorno de producción en un clúster de Kubernetes con Auto Deploy.